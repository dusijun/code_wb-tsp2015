% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% T. Wu and W. U. Bajwa, "Learning the nonlinear geometry of high-dimensional data: Models and algorithms," IEEE Trans. 
% Signal Processing, vol. 63, no. 23, pp. 6229-6244, 2015, Companion Code, ver. 1.0.
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% MIT License
% 
% Copyright (c) [2016] [Tong Wu and Waheed U. Bajwa]
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


% CODE OUTLINE:
% This code is to test the clustering performance using kernel metric-constrained
% union-of-subspaces when the data are complete. In this code, we use Gaussian kernel.



clear all;
close all;
clc

rng('shuffle');
load data_cluster

% parameters
Subn = 2; dim = 35;
param1 = 8; % Gaussian kernel parameter
lambda = 200;
maxiter = 5;subiter = 5;

gt = repmat([1:2],120,1);
gt = gt(:);

% Generate data
Ntrain = 240;
trainidx1 = randperm(200,Ntrain/2);
trainidx2 = randperm(200,Ntrain/2);
    
train = [data(:,trainidx1,1), data(:,trainidx2,2)];
train = train./repmat(sqrt(sum(train.^2)),[size(train,1) 1]);

% Perform algorithm
N = size(train,2);
G = calgram(train, train, 'gauss', param1);

for i = 1:20
    
    [D, subidx, Nl] = MCKUSaL( G, train, dim, Subn, lambda, 'gauss', param1, [], maxiter, subiter );
    trainsubidx = kernelSubspaceAssign(D, train, G, train, Nl, subidx, Subn, 'gauss', param1, []);
    missrate = Misclassification(trainsubidx,gt)
    
end
