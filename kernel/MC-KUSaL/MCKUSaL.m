function [ D, subidx, Nl ] = MCKUSaL( gram, train, s, L, lambda, kernel, param1, param2, maxIter, subiter )

% INPUT:
% gram: gram matrix
% train: training samples, every column corresponds to one signal
% s: dimension of the subspaces
% L: number of subspaces
% lambda: regularization parameter
% kernel: kernel type
% param1, param2: kernel parameters, for Gaussian kernel, param2 = []
% maxIter: maximum number of iterations
% subiter: number of iterations in each subspace update stage
%
% OUTPUT:
% D: bases representation matrices, every dimension in D is a matrix of
% dimension Nl(i)*s
% subidx: the i-th row contains the indexes of signals belong to i-th subspace
% Nl: number of signals belong to each subspace

N = size(gram,2);
H = eye(N) - 1/N*ones(N);
centergram = H*gram*H;
[ D, subidx, Nl ] = GKIOP( centergram, L, s );

for iter = 1:maxIter
    
    % kernel subspace assignment
    subspaceidx = kernelSubspaceAssign(D, train, gram, train, Nl, subidx, L, kernel, param1, param2);
    
    clear subidx Nl D
    subidx = zeros(L,N);  % the i-th row contains the indexes of signals belong to i-th subspace
    Nl = zeros(L,1);    % number of signals belong to each subspace
    D = zeros(N,s,L);  % bases representation matrices
    
    for i = 1:L
        Nl(i) = sum(subspaceidx==i);
        subidx(i,1:Nl(i)) = find(subspaceidx==i);
    end
    % kernel subspace update
    for i = 1:L  %initialize bases
        Gi = centergram(subidx(i,1:Nl(i)),subidx(i,1:Nl(i)));
        [V,sigma] = eig(Gi);
        sigma = diag(sigma);
        [sigma,IX] = sort(sigma,'descend');
        V = V(:,IX);
        sigma = diag(sigma);
        D(1:Nl(i),:,i) = V(:,1:s)* (sigma(1:s,1:s)^(-1/2));
        clear Gi V sigma IX
    end
    
    for count = 1:subiter
        D = kernelSubspaceUpdate(centergram, D, subidx, Nl, L, lambda);
    end
    
    clear subspaceidx
    
end


end

function D = kernelSubspaceUpdate(centergram, D, subidx, Nl, L, lambda)

s = size(D,2);
for i = 1:L
    
    A = zeros(size(Nl(i)));
    for j = 1:L
        if (j~=i)
            Gij = centergram(subidx(i,1:Nl(i)), subidx(j,1:Nl(j)));
            A = A + Gij*D(1:Nl(j),:,j) *D(1:Nl(j),:,j)' *Gij';
            clear Gij
        end
    end
    
    Gi = centergram(subidx(i,1:Nl(i)), subidx(i,1:Nl(i)));
    A = A + lambda/2*Gi*Gi;
    [V,Sigma] = eig(A,Gi,'chol');
    Sigma = diag(Sigma);
    [~,ix] = sort(Sigma,'descend');
    V = V(:,ix);
    U = V(:,1:s);
    U = U./repmat(sqrt(diag(U'*Gi*U)'),[size(U,1) 1]);
    
    D(1:Nl(i),:,i) = U;
    clear A Gij Gi V Sigma U
end

end