function labelidx = kernelSubspaceAssign(D, train, gram, inputsig, Nl, subidx, L, kernel, param1, param2)

% returns the subspace association of each signal

labelidx = zeros(size(inputsig,2),1);
N = size(train,2);
for i = 1:size(inputsig,2)
    residual = zeros(L,1);
    for j = 1:L
        
        k_x = Kernel(inputsig(:,i), train, kernel, param1, param2)';
        k_tilde = Kernel(inputsig(:,i),inputsig(:,i), kernel, param1, param2) - 2/N*ones(1,N)*k_x + 1/(N^2)*ones(1,N)*gram*ones(N,1);
        psi = k_x(subidx(j,1:Nl(j)));
        
        psi_tilde = psi - 1/N*ones(Nl(j),1)*ones(1,N)*k_x - 1/N*gram(subidx(j,1:Nl(j)),:)*ones(N,1) + 1/(N^2)*ones(Nl(j),1)*ones(1,N)*gram*ones(N,1);
        residual(j) = k_tilde - sum((D(1:Nl(j),:,j)'*psi_tilde).^2);
        clear psi k_x k_tilde psi_tilde
    end
    
    [~,labelidx(i)] = min(residual);
    clear residual
end

end