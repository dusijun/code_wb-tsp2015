function labelidx = MisskernelSubspaceAssign_TestStage(D, misstrain, missgram, missinputsig, Nl, subidx, L, kernel, param1, param2)

% returns the subspace association of each signal

labelidx = zeros(size(missinputsig,2),1);
N = size(misstrain,2);
for i = 1:size(missinputsig,2)
    residual = zeros(L,1);
    for j = 1:L
        
        k_x = Appro_kernel(missinputsig(:,i), misstrain, kernel, param1, param2)';
        k_tilde = Appro_kernel(missinputsig(:,i),missinputsig(:,i), kernel, param1, param2) - 2/N*ones(1,N)*k_x + 1/(N^2)*ones(1,N)*missgram*ones(N,1);
        psi = k_x(subidx(j,1:Nl(j)));
        
        psi_tilde = psi - 1/N*ones(Nl(j),1)*ones(1,N)*k_x - 1/N*missgram(subidx(j,1:Nl(j)),:)*ones(N,1) + 1/(N^2)*ones(Nl(j),1)*ones(1,N)*missgram*ones(N,1);
        residual(j) = k_tilde - sum((D(1:Nl(j),:,j)'*psi_tilde).^2);
        clear psi k_x k_tilde psi_tilde
    end
    [~,labelidx(i)] = min(residual);
    clear residual
end

end