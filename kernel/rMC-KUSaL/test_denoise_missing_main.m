% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% T. Wu and W. U. Bajwa, "Learning the nonlinear geometry of high-dimensional data: Models and algorithms," IEEE Trans. 
% Signal Processing, vol. 63, no. 23, pp. 6229-6244, 2015, Companion Code, ver. 1.0.
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% MIT License
% 
% Copyright (c) [2016] [Tong Wu and Waheed U. Bajwa]
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


% CODE OUTLINE:
% We first generate noisy test data and then learn a kernel metric-constrained
% union-of-subspaces from the missing training data. Finally, representation errors of test data 
% are computed based on the pre-image.



clear all;
close all;
clc

rng('shuffle');
load data_denoise

% parameters
Subn = 2; dim = 45;
param1 = 4;
lambda = 4;
maxiter = 5;subiter = 5;

% Generate data
Ntrain = 240;
sigma_test = 0.4; % test noise level
trainidx1 = randperm(200,Ntrain/2);
testidx1 = setdiff(1:200,trainidx1);
trainidx2 = randperm(200,Ntrain/2);
testidx2 = setdiff(1:200,trainidx2);
    
train = [data(:,trainidx1,1), data(:,trainidx2,2)];
test = [data(:,testidx1,1), data(:,testidx2,2)];
    
train = train./repmat(sqrt(sum(train.^2)),[size(train,1) 1]);
test = test./repmat(sqrt(sum(test.^2)),[size(test,1) 1]);
testnoisy = test + randn(size(test))*sqrt(sigma_test/size(test,1));

percent = 0.8;
m = size(train,1);
nobserved = floor(percent*m);
missY = NaN(size(train));
for i = 1:size(missY,2)
    posobserved = randperm(m,nobserved);
    missY(posobserved,i) = train(posobserved,i);
    clear posobserved
end
        

% Perform algorithm
N = size(missY,2);
G = missgram(missY, missY, 'gauss', param1);
[V,sigma] = eig(G);
sigma = diag(sigma);
[sigma,IX] = sort(sigma,'descend');
V = V(:,IX);
idx = find(sigma<0);
sigma(idx) = -sigma(idx);
G = V*diag(sigma)*V';

[D, subidx, Nl] = rMCKUSaL( G, dim, Subn, lambda, maxiter, subiter );

% Denoise test data
testsubidx = MisskernelSubspaceAssign_TestStage(D, missY, G, testnoisy, Nl, subidx, Subn, 'gauss', param1, []);
PI = zeros(size(testnoisy));

for i = 1:size(testnoisy,2)
    class = testsubidx(i);
    Dtau = D(1:Nl(class),:,class);
    signalsinclassidx = subidx(class,1:Nl(class));
    PI(:,i) = MissGaussianMCKUoS_PreImage(G, train, Dtau, signalsinclassidx, testnoisy(:,i), param1);
    clear class signalsinclassidx Dtau
end

error_test = mean(sum(PI-test).^2);
