% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% T. Wu and W. U. Bajwa, "Learning the nonlinear geometry of high-dimensional data: Models and algorithms," IEEE Trans. 
% Signal Processing, vol. 63, no. 23, pp. 6229-6244, 2015, Companion Code, ver. 1.0.
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% MIT License
% 
% Copyright (c) [2016] [Tong Wu and Waheed U. Bajwa]
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


% CODE OUTLINE:
% We first generate noisy synthetic data and then learn a metric-constrained
% union-of-subspaces from the data. Finally, representation errors of test data 
% are computed. This experiment corresponds to Fig.2(a) of the paper.



clear all;
close all;
clc

rng('shuffle');

% parameters
Subn = 5; dim = 13;  % set the number and dimension of subspaces
m = 180; % dimensionality of the signals
n = [150 100 150 100 150]; % The number of points in each subspace
param = 0.04; % this parameter is set to determine the distance between the subspaces

maxiter = 20; % number of iterations
lambda = 2; % regularization parameter

% Generate data
[gtD, train, test] =  GenerateCleanData(m, n, dim, Subn, param);

sigma_train = 0.1; % training noise level
sigma_test = 0.1; % test noise level
train = train./repmat(sqrt(sum(train.^2)),[size(train,1) 1]);
test = test./repmat(sqrt(sum(test.^2)),[size(test,1) 1]);

trainnoisy = train + randn(size(train))*sqrt(sigma_train/m);
testnoisy = test + randn(size(test))*sqrt(sigma_test/m);

% Preprocessing the data, this step is not central to our work and can be removed based upon the property of the data 
ymean = mean(trainnoisy,2);
trainnoisy = trainnoisy - repmat(ymean,[1 size(trainnoisy,2)]);

Dinit = randn(m,dim*Subn); % initialize the orthonormal bases
for i = 1:Subn
    [Dinit(:,(i-1)*dim+1:i*dim),~] = qr( Dinit(:,(i-1)*dim+1:i*dim),0 );
end

D1 = MiCUSaL( trainnoisy, dim, Subn, Dinit, lambda, maxiter );

% denoising test data
temptest = testnoisy - repmat(ymean,[1 size(testnoisy,2)]);
[X1_te,~] = SubassignFindCoeff(D1, temptest, dim, Subn);
Ytesttrace = D1*X1_te + repmat(ymean,[1 size(temptest,2)]);
error_test = mean(sum((test-Ytesttrace).^2)./sum(test.^2));
