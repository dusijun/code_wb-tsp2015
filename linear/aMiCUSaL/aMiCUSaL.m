function [D, estidim, Lhat] = aMiCUSaL( Y, Lmax, smax, epsilon, k1, k2, lambda, maxIter )

% INPUT:
% Y: training data, every column in Y corresponds to one sample
% Lmax: the upper bound of the number of subspaces
% smax: the upper bound of dimension of the subspaces
% epsilon: subspace merging threshold
% k1,k2: parameters used to get the dimension of the subspaces
% lambda: regularization parameter
% maxIter: maximum number of iterations in each step
%
% OUTPUT:
% D: final orthonormal bases, every block corresponds to one subspace
% estidim: estimated dimension of the subspaces
% Lhat: estimated number of subspaces


Lhat = Lmax;  maxdim = smax;
m = size(Y,1);
D = randn(m,maxdim*Lhat); % initialize D
for i = 1:Lhat
    [D(:,(i-1)*maxdim+1:i*maxdim),~] = qr( D(:,(i-1)*maxdim+1:i*maxdim),0 );
end

% perform MiCUSaL first but with a subspace pruning step
for iter = 1:maxIter
    subidx = SubspaceAssign(D, Y, maxdim, Lhat);
    [D,Lhat] = DynamicSubspaceUpdate(Y, D, subidx, maxdim, Lhat, lambda);
end

% merge the subspaces
[D,Lhat] = MergeSubspace(Y, D, subidx, maxdim, Lhat, lambda, epsilon);
% denoise data
[X1_tr,subidx] = SubassignFindCoeff(D, Y, maxdim, Lhat);
Yde = D*X1_tr;
% estimating the dimension of the subspaces
[D,estidim] = SubspaceDimEstimation(Yde, D, subidx, Lhat, k1, k2);
% perform MiCUSaL
for iter = 1:maxIter
    subidx = SubspaceAssign(D, Y, estidim, Lhat);
    D = SubspaceUpdate(Y, D, subidx, estidim, Lhat, lambda);
end

end

function [newD, newL] = DynamicSubspaceUpdate(Data, D, subidx, s, L, lambda)

newL = 0;
oldtonewmatch = [];
for i = 1:L
    relevantDataIndices = find(subidx==i);
    if (length(relevantDataIndices)>=1)
        newL = newL+1;
        oldtonewmatch = [oldtonewmatch i];
    end
end

newsubidx = zeros(size(subidx));
newD = zeros(size(D,1),s*newL);
for i = 1:newL
    sigidx = find(subidx==(oldtonewmatch(i)));
    newsubidx(sigidx) = i;
    newD(:,(i-1)*s+1:i*s) = D(:,(oldtonewmatch(i)-1)*s+1:oldtonewmatch(i)*s);
end

for i = 1:newL
    startidx = (i-1)*s+1;
    relevantDataIndices = find(subidx==i);
    subData = Data(:,relevantDataIndices);
    betterD = FindBetterD(subData,newD,startidx,s,newL,lambda);
    newD(:,startidx:startidx+s-1) = betterD;
end

end




function [D, newL] = MergeSubspace(Data, D, subidx, s, L, lambda, epsilon)

% This function greedily merge subspaces if the distance between them is
% larger than epsilon

dist = zeros(L);
for i = 1:L
    for j = 1:L
        dist(i,j) = sqrt(1- trace(D(:,(i-1)*s+1:i*s)'*D(:,(j-1)*s+1:j*s)*D(:,(j-1)*s+1:j*s)'*D(:,(i-1)*s+1:i*s))/s );
    end
end
for i = 1:L
    dist(i,i) = 100;
end

newL = L;
minvalue = min(dist(:));

while (minvalue <= epsilon)
    
    newL = newL-1;
    [p,q] = find(dist==minvalue);
    p = p(1); q = q(1);
    pp = max(p,q); qq = min(p,q);
    
    relevantDataIndices = find(subidx==pp);
    subidx(relevantDataIndices) = qq;
    for i = pp+1:newL+1
        relevantDataIndices = find(subidx==i);
        subidx(relevantDataIndices) = i-1;
        clear relevantDataIndices
    end
    newD = zeros(size(D,1),s*newL);
    newD(:,1:(qq-1)*s) = D(:,1:(qq-1)*s);
    newD(:,qq*s+1:end) = [D(:,qq*s+1:(pp-1)*s) D(:,pp*s+1:end)];
    
    startidx = (qq-1)*s+1;
    relevantDataIndices = find(subidx==qq);
    subData = Data(:,relevantDataIndices);
    newD(:,(qq-1)*s+1:qq*s) = FindBetterD(subData,newD,startidx,s,newL,lambda);
    D = newD;
    clear newD
    
    dist = zeros(newL);
    for i = 1:newL
        for j = 1:newL
            dist(i,j) = sqrt(1- trace(D(:,(i-1)*s+1:i*s)'*D(:,(j-1)*s+1:j*s)*D(:,(j-1)*s+1:j*s)'*D(:,(i-1)*s+1:i*s))/s );
        end
    end
    for i = 1:newL
        dist(i,i) = 100;
    end
    
    minvalue = min(dist(:));
end


end




function D = SubspaceUpdate(Data, D, subidx, s, L, lambda)

for i = 1:L
    startidx = (i-1)*s+1;
    relevantDataIndices = find(subidx==i);
    if (length(relevantDataIndices)<1)
        betterD = D(:,bstartidx:bstartidx+dim-1);
    else
        subData = Data(:,relevantDataIndices);
        betterD = FindBetterD(subData,D,startidx,s,L,lambda);
    end
    D(:,startidx:startidx+s-1) = betterD;
end

end


function betterD = FindBetterD(DatainSub, D, startidx, s, L, lambda)

D0idx = setdiff(1:size(D,2),startidx:startidx+s-1);
D0 = D(:,D0idx);
S = zeros(size(D,1));
for i = 1:L-1
    Di = D0(:,(i-1)*s+1:i*s);
    S = S + Di*Di';
end
S = S + lambda/2*DatainSub*DatainSub';

[U,Sigma] = eig(S);
Sigma = diag(Sigma);
[~,ix] = sort(Sigma,'descend');
U = U(:,ix);
betterD = U(:,1:s);

end


function subidx = SubspaceAssign(D, Y, s, L)

coher = zeros(L,size(Y,2));
for i = 1:L
    tempD = D(:,(i-1)*s+1:i*s);
    tempproj = tempD'*Y;
    coher(i,:) = sum(tempproj.^2,1);
    clear tempproj
end
[~, subidx] = max(coher,[],1);

end