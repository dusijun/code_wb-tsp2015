function D = rMiCUSaL( missY, s, L, Dinit, lambda, stepsize, maxCycles, maxIter )

% INPUT:
% Y: training data with missing entries, every column in Y corresponds to one sample
% s: dimension of the subspaces
% L: number of subspaces
% Dinit: initial D
% lambda: regularization parameter
% stepsize: the parameter for stochastic gradient descent step size
% maxCycles: number of passes over the data in each iteration
% maxIter: maximum number of iterations
%
% OUTPUT:
% D: final orthonormal bases, every block corresponds to one subspace

D = Dinit;
for iter = 1:maxIter
    subidx = MissingSubspaceAssign(D, missY, s, L);
    D = SubspaceUpdateMiss(missY, D, subidx, s, L, stepsize, maxCycles, lambda);
end


end


function D = SubspaceUpdateMiss(MissingData, D, subidx, s, L, step_size, maxCycles, lambda)

for i = 1:L
    startidx = (i-1)*s+1;
    relevantDataIndices = find(subidx==i);
    
    if (length(relevantDataIndices)<1)
        betterD = D(:,startidx:startidx+s-1);
    else
        Y = MissingData(:,relevantDataIndices);
        U = D(:,startidx:startidx+s-1);
        D0idx = setdiff(1:size(D,2),startidx:startidx+s-1);
        D0 = D(:,D0idx);
        A = 0;
        for j = 1:L-1
            Di = D0(:,(j-1)*s+1:j*s);
            A = A + Di*Di';
        end
        betterD = FindBetterDMiss(Y, A, step_size, maxCycles, U, lambda/2);
        clear U Y A
    end
    D(:,startidx:startidx+s-1) = betterD;
end

end


function U = FindBetterDMiss(Data, A, step_size, maxCycles, Uinit, lambda)

% INPUT:
% Data: incomplete data (with 0 at the non-observed position)
% A: sum of the projection matries of all the other subspaces
% step_size: the constant for stochastic gradient descent step size
% maxCycles: number of passes over the data
% Uinit: an initial guess for the column space U
% lambda: regularization parameter
%
% OUTPUT:
% U: optimized orthonormal basis

Indicator = ~isnan(Data);
U = Uinit;

sigidx = find(Indicator(:,1));
neww = size(Data,1)/length(sigidx);
step_size = step_size/lambda/neww;

for outiter = 1:maxCycles
    
    oldU = U;
    
    % minimize -tr(U'AU)
    dF = 2*(eye(size(U,1)) - U*U')*A*U;
    [UU,SS,VV] = svd(dF,'econ');
    U = U*VV*diag(cos(step_size/outiter*diag(SS)))*VV' + UU*diag(sin(step_size/outiter*diag(SS)))*VV';
    
    for k = 1:size(Data,2)
        
        % Pull out the relevant indices and revealed entries for this column
        sigidx = find(Indicator(:,k));
        neww = size(Data,1)/length(sigidx);
        v_Omega = Data(sigidx,k);
        U_Omega = U(sigidx,:);
        
        weights = U_Omega\v_Omega;
        norm_weights = norm(weights);
        residual = v_Omega - U_Omega*weights;
        norm_residual = norm(residual);
        expandedresidual = zeros(size(U,1),1);
        expandedresidual(sigidx) = residual;
        
        sG = 2*norm_residual*norm_weights;
        t = step_size*neww*sG*lambda/outiter;
        
        % Take the gradient step.
        if t<pi/2 % drop big steps
            alpha = (cos(t)-1)/norm_weights^2;
            beta = sin(t)/sG;
            
            U = U + U*(alpha*weights)*weights' + 2*beta*expandedresidual*weights';
        end
        clear expandedresidual sigidx v_Omega U_Omega weights residual sG t alpha beta neww norm_residual norm_weights
    end
    
    a = norm(oldU*oldU'-U*U','fro');
    if (a<=1e-6) break; end;
    
    clear dF UU SS VV
end


end


function subidx = MissingSubspaceAssign(D, missY, s, L)

r = zeros(L,size(missY,2));
for i = 1:L
    for j = 1:size(missY,2)
        idx = find(~isnan(missY(:,j)));
        tempD = D(idx,(i-1)*s+1:i*s);
        w = tempD\missY(idx,j);
        r(i,j) = sum((missY(idx,j) - tempD*w).^2);
        clear idx tempD w
    end
end
[~, subidx] = min(r,[],1);

end