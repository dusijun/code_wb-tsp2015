function [gtD, Xtrain, Xtest] = GenerateCleanData(m, n, s, L, param)

% INPUT:
% m: dimentionality of the signals
% n: the number of points in each subspace
% s: dimension of the subspaces
% L: number of subspaces
% param: this parameter is set to determine the distance between the
% subspaces, which corresponds to t_s in the paper
%
% OUTPUT:
% gtD: the ground truth orthonormal bases of the subspaces, each dimension
% corresponds to one subspace
% Xtrain: clean training data
% Xtest: clean test data


D = randn(m,s);
[D,~] = qr(D,0);

gtD = zeros(m,s,L);
gtD(:,:,1) = D;
for i = 2:L
    [gtD(:,:,i),~] = qr(gtD(:,:,i-1) + param*rand(m,s),0);  %T_{\ell} = orth(T_{\ell-1}+ t_s W_{\ell})%
end

Xtrain = zeros(m,sum(n));
Xtest = zeros(m,sum(n));

count = 1;
for i = 1:L
    coeff1 = randn(s,n(i));
    Xtrain(:,count:count+n(i)-1) = gtD(:,:,i)*coeff1;
    coeff2 = randn(s,n(i));
    Xtest(:,count:count+n(i)-1) = gtD(:,:,i)*coeff2;
    count = count + n(i);
    clear coeff1 coeff2
end

end