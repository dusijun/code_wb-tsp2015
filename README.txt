Version
--------
Companion Code Version: 1.0


License
--------
This companion code is for:

[1] T. Wu and W.U. Bajwa, "Learning the nonlinear geometry of high-dimensional data: Models and algorithms," IEEE Trans. Signal Processing, vol. 63, no. 23, pp. 6229-6244, Dec. 2015.

This code is being licensed under The MIT License (MIT), Copyright (c) 2016, Tong Wu and Waheed U. Bajwa

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Citation
---------
Any part of this code used in your work should be cited as follows:

T. Wu and W. U. Bajwa, "Learning the nonlinear geometry of high-dimensional data: Models and algorithms," IEEE Trans. Signal Processing, vol. 63, no. 23, pp. 6229-6244, 2015, Companion Code, ver. 1.0.


Reporting of issues
--------------------
Any issues in this code should be reported to T. Wu and W.U. Bajwa. However, this companion code is being provided on an "As IS" basis to support the ideals of reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.


Computational environment
--------------------------
This code has been tested in the following computational environments. While it can run in other environments also, we can neither provide such guarantees nor can help you make it compatible in other environments.

* PC: Windows 7 with Matlab R2015a and Matlab R2016b


Instructions
-------------
This software package contains the Matlab code for metric-constrained union-of-subspaces (MC_UoS) learning in both the ambient space and the kernel space. The directory 'linear' has three folders; each one of these folders corresponds to one learning algorithm operating in the ambient space. The directory 'kernel' has two folders; each one of these folders corresponds to one learning algorithm operating in the kernel space. 

The package also provides an implementation of the algorithm described in [L. Balzano, A. Szlam, B. Recht, and R. Nowak, “K-subspaces with missing data,” in Proc. IEEE Statist. Signal Process. Workshop (SSP), 2012, pp. 612–615.] within the 'ksubmissing' directory.

1. The directory 'MiCUSaL' under 'linear' contains the code for MC-UoS learning in the ambient space using complete data, when the number and dimension of the subspaces are given. The script 'synthetic_complete_main.m' can be used as an example of the usage for this algorithm.

2. The directory 'aMiCUSaL' under 'linear' contains the code for MC-UoS learning in the ambient space using complete data, when the number and dimension of the subspaces are not known a priori.  The script 'sanfran_complete_main.m' can be used as an example of the usage for this algorithm; this script uses the San Francisco City Hall image data, which is provided in data.mat.

3. The directory 'rMiCUSaL' under 'linear' contains the code for MC-UoS learning in the ambient space using missing data, when the number and dimension of the subspaces are given. The script 'synthetic_missing_main.m' can be used as an example of the usage for this algorithm.

4. The directory 'MC-KUSaL' under 'kernel' contains the code for MC-UoS learning in the kernel space using complete data, when the number and dimension of the subspaces are given. The performance of this algorithm is sensitive to initialization, so it is best to have several different runs of this algorithm before selecting the final result.

Usage:
-------
** test_denoise_complete_main.m -- This script can be used as an example for image denoising, where the data is provided in data_denoise.mat.
** test_cluster_complete_main_gauss.m -- This script can be used as an example for clustering using Gaussian kernel, where the data is provided in data_cluster.mat.
** test_cluster_complete_main_poly.m -- This script can be used as an example for clustering using Polynomial kernel, where the data is provided in data_cluster.mat.

5. The directory 'rMC-KUSaL' under 'kernel' contains the code for MC-UoS learning in the kernel space using missing data, when the number and dimension of the subspaces are given. The performance of this algorithm is sensitive to initialization, so it is best to have several different runs of this algorithm before selecting the final result.

Usage:
-------
** test_denoise_missing_main.m -- This script can be used as an example for image denoising, where the data is provided in data_denoise.mat.
** test_cluster_missing_main_gauss.m -- This script can be used as an example for clustering using Gaussian kernel, where the data is provided in data_cluster.mat.
** test_cluster_missing_main_poly.m -- This script can be used as an example for clustering using Polynomial kernel, where the data is provided in data_cluster.mat.

6. The directory 'ksubmissing' contains an implementation of the algorithm described in [L. Balzano, A. Szlam, B. Recht, and R. Nowak, “K-subspaces with missing data,” in Proc. IEEE Statist. Signal Process. Workshop (SSP), 2012, pp. 612–615.] for learning a union-of-subspaces using missing data. The script 'synthetic_missing_main.m'  can be used as an example of the usage for this algorithm.

Additional Copyright Notice:
-----------------------------
Parts of our implementation for 'ksubmissing' include the Matlab code for the GROUSE algorithm, obtained from http://sunbeam.ece.wisc.edu/grouse/